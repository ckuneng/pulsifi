import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable, of, Subject, throwError } from "rxjs";
import { catchError, debounceTime, map, switchMap } from "rxjs/operators";

@Injectable({ providedIn: 'root' })
export class ListService {

    private listUrl = 'https://api.coingecko.com/api/v3/exchange_rates';
    private LIST = [];

    private _search$ = new Subject<void>();
    private _list$ = new BehaviorSubject<any[]>([]);
    private _total$ = new BehaviorSubject<number>(0);
    private _typeList$ = new BehaviorSubject<string[]>([]);
    private _range$ = new BehaviorSubject<any>({ min: 0, max: 0});

    private _state = {
        page: 1,
        pageSize: 10,
        searchTerm: '',
        type: '',
        range: [0, 0],
        sortColumn: '',
        sortDirection: ''
    };

    constructor(private http: HttpClient) {
        this.getList().subscribe(res => {
            this.LIST = res;

            this._typeList$.next(res.reduce((acc: any[], cur: any) => {
                if (!acc.includes(cur.type)) {
                    acc.push(cur.type);
                }

                return acc;
            }, []));

            let temp = res.reduce((acc: any, cur: any) => {
                acc.min = acc.min > cur.value ? Math.floor(cur.value) : acc.min;
                acc.max = acc.max < cur.value ? Math.ceil(cur.value) : acc.max;

                return acc;
            }, { min: 0, max: 0})

            this._range$.next(temp);
            this.range[1] = temp.max;

            this._search$.pipe(
                debounceTime(200),
                switchMap(() => this._search())
            ).subscribe((result: any) => {
                console.log(result);
                this._list$.next(result.list);
                this._total$.next(result.total);
            });
    
            this._search$.next();
        });
    }

    private getList(): Observable<any> {
        return this.http.get(this.listUrl)
            .pipe(
                map((res: any) => {
                    return Object.keys(res.rates).map(key => res.rates[key]);;
                }),
                catchError(error => {
                    return throwError(error);
                })
            );
    }

    get list$() { return this._list$.asObservable(); }
    get total$() { return this._total$.asObservable(); }
    get typeList$() { return this._typeList$.asObservable(); }
    get range$() { return this._range$.asObservable(); }
    get page() { return this._state.page; }
    get pageSize() { return this._state.pageSize; }
    get searchTerm() { return this._state.searchTerm; }
    get type() { return this._state.type; }
    get range() { return this._state.range; }

    set page(page: number) { this._set({ page }); }
    set pageSize(pageSize: number) { this._set({ pageSize }); }
    set searchTerm(searchTerm: string) { this._set({ searchTerm }); }
    set type(type: string) { this._set({ type }); }
    set range(range: number[]) { this._set({ range }); }
    set sortColumn(sortColumn: any) { this._set({ sortColumn }); }
    set sortDirection(sortDirection: any) { this._set({ sortDirection }); }

    private _set(patch: Partial<any>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _search(): Observable<any> {
        const { sortColumn, sortDirection, pageSize, page, searchTerm, type, range } = this._state;

        // 1. sort
        let list = this.sort(this.LIST, sortColumn, sortDirection);

        // 2. filter
        // Text filter
        if (searchTerm) {
            list = list.filter(item => item.name.toLowerCase().includes(searchTerm) || item.unit.toLowerCase().includes(searchTerm));
        }

        // Type filter
        if (type) {
            list = list.filter(item => item.type.toLowerCase() == type);
        }
        
        // Update min & max range according to filter
        this._range$.next(list.reduce((acc: any, cur: any) => {
            acc.min = acc.min > cur.value ? Math.floor(cur.value) : acc.min;
            acc.max = acc.max < cur.value ? Math.ceil(cur.value) : acc.max;
            return acc;
        }, { min: 0, max: 0}));

        // Range filter
        if (range[0] > 0) {
            list = list.filter(item => item.value > range[0]);
        }

        if (range[1] > 0) {
            list = list.filter(item => item.value < range[1]);
        }

        const total = list.length;

        // 3. paginate
        list = list.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
        
        return of({ list: list, total: total });
    }

    private sort(list: any[], column: string, direction: string) {
        if (direction === '' || column === '') {
            return list;
          } else {
            return [...list].sort((a, b) => {
              const res = a[column] < b[column] ? -1 : a[column] > b[column] ? 1 : 0;
              return direction === 'asc' ? res : -res;
            });
          }
    }
}
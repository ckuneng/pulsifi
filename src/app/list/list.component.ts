import { Component } from '@angular/core';
import { Observable } from 'rxjs';

import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';

import { ListService } from './list.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss']
})
export class ListComponent {

    list$: Observable<any[]>;
    total$: Observable<number>;
    typeList$: Observable<any[]>;
    range$: Observable<any>;
    sortCol: string = '';
    sortDir: string = '';
    sortIcon: any = {
        asc: faArrowDown,
        dsc: faArrowUp
    }

    constructor(public listService: ListService) {
        this.list$ = listService.list$;
        this.total$ = listService.total$;
        this.typeList$ = listService.typeList$;
        this.range$ = listService.range$;
    }

    sort(column: string) {
        if (column != this.sortCol) {
            this.sortCol = column;
            this.sortDir = 'asc';
        } else if (this.sortDir == 'asc') {
            this.sortDir = 'dsc'
        } else {
            this.sortCol = '';
            this.sortDir = '';
        }

        this.listService.sortColumn = this.sortCol;
        this.listService.sortDirection = this.sortDir;
    }

    range(event: any) {
        this.listService.range = event;
    }
}